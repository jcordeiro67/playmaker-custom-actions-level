/*--- __ECO__ __PLAYMAKER__ __ACTION__ ---*/

/* 
 Taken from the original Playmaker Action: Restart Level (RestartLevel.cs)
 Updated for Untiy 5 Now uses the SceneManagement LoadScene as opposed to 
 Application LoadLevel which is now obsolete.

Updated by: John Cordeiro
WebSite: johncordeiro.com
 
*/

using UnityEngine;
using UnityEngine.SceneManagement;

namespace HutongGames.PlayMaker.Actions{
	
	[ActionCategory(ActionCategory.Level)]
	[Tooltip("Restart Level for Unity 5. Uses the new SceneManagement method of LoadScene")]
	public class LevelRestart : FsmStateAction
	{

		// Code that runs on entering the state.
		public override void OnEnter()
		{
			SceneManager.LoadScene(SceneManager.GetActiveScene ().buildIndex);
			Finish();
		}

	}
}
